/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_clear.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ourgot <ourgot@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 06:49:27 by ourgot            #+#    #+#             */
/*   Updated: 2020/03/10 10:28:33 by ourgot           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "vectorobj.h"

void	vector_clear(t_obj *vector)
{
	t_meta *meta;

	meta = vector->meta;
	if (vector->dtor)
		foreach(vector, vector->dtor);
	free(meta->data);
	meta->size = 0;
	meta->capacity = 0;
	meta->data = NULL;
}
